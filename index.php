<?php
	if( isset($_GET['peticion']) ) {

		if( $_GET['peticion'] == "seleccionar_materia" ) {
			include_once("./Modelos/seleccionar_materia.php");
			die();
		}
		else if( $_GET['peticion'] == "buscar_materias" ) {
			include_once("./Modelos/lista_materias.php");
			die();
		}
	}
	else if( isset($_POST['peticion']) ) {
		if( $_POST['peticion'] == "buscar_horarios" ) {
			include_once("./Modelos/horarios.php");
			die();
		}
	}
	else {
		include_once("./Vistas/bienvenida.html");
		die();
	}
?>
