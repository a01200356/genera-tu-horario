<?php
	include_once("Modelos/conexion_bd.php");
	mb_internal_encoding('UTF-8');

	if( !isset($_POST['materias']) ) {
		echo json_encode("");
		die();
	}
	
	$materias = json_decode($_POST['materias'],true);

	// La consulta por sí sola no regresa nada...
	$consulta = "SELECT Materias.idMateria, Grupos.idGrupo, Clases.dia, Clases.horaInicio, Clases.horaFin,
										Materias.nombre, Grupos.profesor, Clases.salon
							FROM Materias INNER JOIN Grupos
									ON(Materias.idMateria = Grupos.idMateria)
								INNER JOIN Clases
									ON(Grupos.idGrupo = Clases.idGrupo)
	 						WHERE Materias.idMateria = -1";

	// ...pero con esto ya incluye cada materia seleccionada.
	foreach($materias as $m) {
		$consulta .= " OR Materias.idMateria = " .$m['idMateria'];
	}

	//Busca todas las clases seleccionadas por el alumno.
	$lista_clases = runquery($consulta);

	echo json_encode($lista_clases);
?>