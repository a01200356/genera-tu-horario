<?php

	// Agregar archivo con JSON con los datos de conexión para la base de datos.
	// Aguas en no subir ese archivo al repo, lol.
	function connect() {
		$sitio_db = json_decode(file_get_contents("Modelos/.sitio_bd"), true);
		$mysql = mysqli_connect($sitio_db['host'], $sitio_db['usuario'], $sitio_db['contraseña'], $sitio_db['base_de_datos']);
		mysqli_set_charset($mysql, "utf8");
		return $mysql;
	}
	
	function disconnect($mysql) {
		mysqli_close($mysql);
	}
	
	//$params must always be an array.
	//Returns array with results if select, returns boolean otherwise.
	function runquery($query, $types = null, $params = null) {
		$mysql = connect();
		
		if(!($statement = $mysql->prepare($query))) {
			echo "<strong>Preparation failed:</strong> (" . $mysql->errno . ") " . $mysql->error;
			return false;
		}
		if($types != null && $params != null) {
			$bind_names[] = $types;
			for ($i = 0; $i < count($params); $i++) {
				$bind_name = 'bind' . $i;
				$$bind_name = $params[$i];
				$bind_names[] = &$$bind_name;
            		}
            		call_user_func_array(array($statement,'bind_param'),$bind_names);
        	}
		if(!$statement->execute()) {
			echo "<strong>Execution failed:</strong> (" . $statement->errno . ") " . $statement->error;
			return false;
		}
		
		$metadata = mysqli_stmt_result_metadata($statement);
		
		if(!$metadata) return null;
		
		$nCols = mysqli_num_fields($metadata);
		
		$results_array = array();
		
		while ($field = $metadata->fetch_field()) { 
			$cols[] = &$row[$field->name]; 
		} 
		
		call_user_func_array(array($statement, 'bind_result'), $cols);            
		while ($statement->fetch()) {
			foreach($row as $key => $val) { 
				$c[$key] = $val; 
			} 
			$results_array[] = $c; 
		} 
		
		disconnect($mysql);
		return $results_array;
	}
	
?>