<?php
	include_once("Modelos/conexion_bd.php");
	mb_internal_encoding('UTF-8');

	// Busca todas las materias disponibles.
	$materias = runquery("SELECT nombre FROM Materias");

	echo json_encode($materias);
?>