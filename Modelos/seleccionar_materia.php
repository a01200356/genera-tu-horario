<?php
	include_once("Modelos/conexion_bd.php");
	mb_internal_encoding('UTF-8');

	if(isset($_GET['materia'])) {
		$materia = runquery("SELECT Materias.idMateria, nombre, count(idGrupo) as numGrupos
			FROM Materias LEFT JOIN Grupos ON(Materias.idMateria = Grupos.idMateria)
			WHERE nombre LIKE ?
			GROUP BY Materias.idMateria", 's', array($_GET['materia']));

        echo json_encode($materia);
	}
	else {
		echo json_encode(array());
	}
?>
