
lista_horarios = []; //Contiene todas las combinaciones válidas de horarios que se pueden generar. Es un arreglo de listas de grupos.
iHorarios = 0; //El último índice de horario creado.
materias = []; //Lista de materias seleccionadas.
colores = {}; //Arreglo asociativo entre materias y su color seleccionado.
grupos = []; //Lista de todos los grupos disponibles con su número de materia.
lista_clases = []; //Lista de todas las clases con su grupo, materia y toda la información para desplegar en la tabla.
minClases = 2; //El número mínimo de clases que el alumno puede inscribir.
maxClases = 8; //El número máximo de clases que el alumno puede inscribir.

$(function(){
    $("#generar_horarios").click(function(){
		horarios();
    });
});

// Esta es la función que se llama cuando se hace click en el botón de 'Generar horarios' e inicia todo lo demás.
function horarios() {

	// Reiniciar todas las variables y estructuras globales.
	lista_horarios = [];
	iHorarios = 0;
	materias = [];
	colores = {};
	grupos = [];
	lista_clases = [];

	$('#horarios').html("Generando horarios... ");
        materias_seleccionadas();
        $.ajax({
            type: "POST",
            url: "./",
            data: {
		        peticion: "buscar_horarios",
                materias: JSON.stringify(materias),
            }
        })
		.done(function(resultados) {
            clases = JSON.parse(resultados);
			llenar_grupos(clases);
			for(var i = minClases; i <= maxClases; i++) {
				generar_horarios(0, i, new Array());
				eliminar_empalmes();
			}
			dibujar_horarios();
			if(grupos.length > 0) {
				filtrar_horarios();
			}
			return true;
        });
}

//Dibuja todos los horarios.
function dibujar_horarios() {
	if(grupos.length == 0) {
		var mensaje = "<p>No se han generado horarios porque aún no has agregado materias.</p>";
		$('#mensaje').html(mensaje);
		$('#horarios').html("<button id=\"generar_horarios\" name=\"generar_horarios\" onclick=\"horarios()\">Generar horarios</button>");
		return true;
	}
	var tablas_html = "<button id=\"generar_horarios\" name=\"generar_horarios\" onclick=\"horarios()\">Generar horarios</button>\n";
	tablas_html += "<p>Estos son tus posibles horarios con las materias que seleccionaste:</p>";
	tablas_html += dibujar_selector_clases();
	for(var i in lista_horarios) {
		tablas_html += "<div class='horario' id='horario" + i + "' name='horario" + i + "'>\n";
		tablas_html += crear_tabla(i);
		tablas_html += "</div>\n";
	}
	$('#horarios').html(tablas_html);
	
	//Rellena cada tabla con los cuadritos de colores.
	for(var i in lista_horarios) {
		for(var j in lista_horarios[i]) {
			for(var k in lista_clases) {
				if(lista_horarios[i][j].idGrupo == lista_clases[k].idGrupo) {
					var dia = parseInt(lista_clases[k].horaInicio/10000);
					var horaInicio = (lista_clases[k].horaInicio%10000)/30 - 12;
					var duracion = (lista_clases[k].horaFin - lista_clases[k].horaInicio)/30;
					for(var z = 0; z < duracion; z++) {
						if((horaInicio+z+1)%2 == 0) {
							document.getElementById('horario_tabla'+i).rows[horaInicio+z+1].cells[dia-1].innerHTML = lista_clases[k].salon;
							document.getElementById('horario_tabla'+i).rows[horaInicio+z+1].cells[dia-1].style.backgroundColor = colores[lista_clases[k].idMateria];
						}
						else {
							document.getElementById('horario_tabla'+i).rows[horaInicio+z+1].cells[dia].innerHTML = lista_clases[k].salon;
							document.getElementById('horario_tabla'+i).rows[horaInicio+z+1].cells[dia].style.backgroundColor = colores[lista_clases[k].idMateria];
						}
					}
					if(document.getElementById('horario_info'+i).rows[2*j].cells[1].innerHTML == "") {
						document.getElementById('horario_info'+i).rows[2*j].cells[0].style.backgroundColor = colores[lista_clases[k].idMateria];
						document.getElementById('horario_info'+i).rows[2*j].cells[1].innerHTML = "<strong>" + lista_clases[k].nombre + "</strong>";
						document.getElementById('horario_info'+i).rows[2*j+1].cells[1].innerHTML = lista_clases[k].profesor;
					}
				}
			}
		}
	}
	return true;
}

//Dibuja el selector de número de clases que aparecerán en la pantalla. El número por default es 5.
function dibujar_selector_clases() {
	var selector_html = "<label for='numClases'>Número de clases: </label><select id='numClases' name='numClases' onchange='filtrar_horarios()'>";
	for(var i = minClases; i <= maxClases; i++) {
		selector_html += "<option id='" + i + "' name='" + i + "'";
		if(i == 5) selector_html += " selected='selected'";
		selector_html += ">" + i + "</option>\n";
	}
	selector_html += "</select>\n";
	return selector_html;
}

//Dibuja una nueva tabla html con un horario vacío.
function crear_tabla(numHorario) {
	var tabla_html = "<table class='table table-bordered horario_tabla' id='horario_tabla" + numHorario + "' name='horario_tabla" + numHorario + "'>\n";
	tabla_html += "<thead><th></th><th>Lunes</th><th>Martes</th><th>Miércoles</th><th>Jueves</th><th>Viernes</th><th>Sábado</th><th>Domingo</th></thead>\n";
	for(var i = 0; i < 36; i++) {
		tabla_html += "<tr>";
		if(i%2 == 0) {
			tabla_html += "<td rowspan='2'>";
			if(i < 8) {
				tabla_html += "0";
			}
			tabla_html += (6 + parseInt(i/2)) + ":00 hrs.</td>";
		}
		for(var j = 0; j < 7; j++) {
			tabla_html += "<td>-</td>";
		}
		tabla_html += "</tr>\n";
	}
	tabla_html += "</table>\n";
	tabla_html += "<table class='horario_info' id='horario_info" + numHorario + "' name='horario_info" + numHorario + "'>\n"
	for(var i in lista_horarios[numHorario]) {
		tabla_html += "<tr><td width='5%'></td><td></td></tr><tr><td></td><td></td></tr>\n";
	}
	tabla_html += "</table>\n";
	return tabla_html;
}

//Genera todas las combinaciones de grupos sin materias repetidas y las escribe en el arreglo global horarios.
function generar_horarios(iGrupos, nCombinaciones, horario) {
	if(nCombinaciones == 1) {
		for(var i = iGrupos; i < grupos.length; i++) {
			horario.push(grupos[i]);
			if(!materia_repetida(horario)) {
				lista_horarios[iHorarios] = new Array();
				for(var j in horario) {
					lista_horarios[iHorarios].push(horario[j]);
				}
				iHorarios++;
			}
			horario.pop();
		}
	}
	else {
		for(var i = iGrupos; i <= grupos.length-nCombinaciones; i++) {
			horario.push(grupos[i]);
			generar_horarios(i+1, nCombinaciones-1, horario);
			horario.pop();
		}
	}
	return true;
}

//Verifica si el horario generado contiene alguna materia repetida.
function materia_repetida(horario) {
	for(var i in horario) {
		for(var j in horario) {
			if(i != j && horario[i].idMateria == horario[j].idMateria) {
				return true;
			}
		}
	}
	return false;
}

//Elimina los horarios que tienen clases que empalman.
function eliminar_empalmes() {
	var lista_empalmes = [];
	for(var i in lista_horarios) {
		if(empalma(lista_horarios[i])) {
			lista_empalmes.push(i);
		}
	}
	for(var i = lista_empalmes.length-1; i >= 0; i--) {
		lista_horarios.splice(lista_empalmes[i], 1);
		iHorarios--;
	}
	return true;
}

//Regresa verdadero si el horario tiene un emplame en alguna de sus clases.
function empalma(horario) {
	var clases = [];
	for(var i in horario) {
		for(var j in lista_clases) {
			if(lista_clases[j].idGrupo == horario[i].idGrupo) {
				clases.push(j);
			}
		}
	}
	for(var j in clases) {
		for(var k in clases) {
			if(j != k && (lista_clases[clases[j]].horaInicio >= lista_clases[clases[k]].horaInicio && lista_clases[clases[j]].horaInicio < lista_clases[clases[k]].horaFin)) {
				return true;
			}
		}
	}
	return false;
}

//Llena el arreglo global grupos según las clases dadas.
function llenar_grupos(clases) {
	var nuevo_grupo = true;
	for(var i in clases) {
		for(var j in grupos) {
			if(clases[i].idGrupo == grupos[j].idGrupo) {
				nuevo_grupo = false;
				break;
			}
		}
		if(nuevo_grupo) {
			grupos.push({idGrupo: clases[i].idGrupo, idMateria: clases[i].idMateria});
		} else {
			nuevo_grupo = true;
		}
        clases[i].horaInicio = tiempo(clases[i].dia, clases[i].horaInicio);
        clases[i].horaFin = tiempo(clases[i].dia, clases[i].horaFin);
        lista_clases.push(clases[i]);
	}
	return true;
}

//Convierte las fechas en un número entero.
//El primer dígito indicando el día de la semana (1 - 7) y los demás dígitos indicando los minutos del día (hasta 1440).
function tiempo(dia, hora) {
    //alert(dia + ":" + hora);
	var h = hora.split(":");
	var d;
	switch(dia) {
		case 'Lu':
			d = 1;
			break;
		case 'Ma':
			d = 2;
			break;
		case 'Mi':
			d = 3;
			break;
		case 'Ju':
			d = 4;
			break;
		case 'Vi':
			d = 5;
			break;
		case 'Sa':
			d = 6;
			break;
		case 'Do':
			d = 7;
			break;
	}
	return d*10000 + parseInt(h[0])*60 + parseInt(h[1]);
}

//Hace que sólo sean visibles los horarios con el número de materias especificado.
function filtrar_horarios() {
	var numClases = document.getElementById("numClases").value;
	var contadorHorarios = 0;
	for(var i in lista_horarios) {
		if(lista_horarios[i].length != numClases) {
			document.getElementById('horario'+i).style.display= "none";
		}
		else {
			document.getElementById('horario'+i).style.display= "block";
			contadorHorarios++;
		}
	}
	if(contadorHorarios == 0) {
		var mensaje = "<p>No tienes horarios disponibles con el número de clases especificados. Puedes cambiar el número de clases en el selector de arriba o agregar más materias.</p>";
		$('#mensaje').html(mensaje);
	}
	else {
		$('#mensaje').html("");
	}
	return true;
}

//Obtiene las materias seleccionadas por el usuario.
function materias_seleccionadas() {
    var i = 0;
    $('#tabla_materias > tbody > tr').each(function() {
        var materia = new Object();
        materia.idMateria = $(this).attr('name');
        colores[materia.idMateria] = $("#color_"+materia.idMateria).val();
        materias[i] = materia;
        i++;
    })
}
