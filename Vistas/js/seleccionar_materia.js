
materias = []; // Arreglo con todas las materias disponibles, para autorelleno.

$(function(){
    $("#agregar_materia").click(seleccionar_materia);
    $("#nombre_materia").keydown(function(e){if(e.which==13)seleccionar_materia()});
    materias_disponibles();
    $("#nombre_materia").autocomplete({
        source: materias
    });
});

function materias_disponibles() {
	$.ajax({
		type: "GET",
		url: "./",
		data: {
			peticion: "buscar_materias"
		}
	})
	.done(function(resultado) {
		var r = JSON.parse(resultado);
		for(var i = 0; i < r.length; i++) {
			materias[i] = r[i]['nombre'];
		}
		
	});
}

// Selecciona color
function selecciona_color() {
	var caracteres = '0123456789ABCDEF'.split('');
	var color = '#';
	for (var i = 0; i < 6; i++) {
		color += caracteres[Math.floor(Math.random() * 16)];
	}
	return color;
}

// Busca la materia en la base y si la encuentra escribe la fila correspondiente en la tabla.
function seleccionar_materia() {
	var nombre = $("#nombre_materia").val();
	$("#nombre_materia").val("");
	$.ajax({
		type: "GET",
		url: "./",
		data: {
			peticion: "seleccionar_materia",
			materia: String(nombre)
		}
	})
	.done(function(resultado) {
            var m = JSON.parse(resultado);
            var filas = "";
            for(i = 0; i < m.length; i++) {
                if( $("#fila_"+m[i].idMateria).length == 0 ) {
                    filas += "<tr id=\"fila_" + m[i].idMateria + "\" name=\"" + m[i].idMateria + "\">";
                    filas += "<td><input type=\"color\" id=\"color_" + m[i].idMateria + "\" value=\"" + selecciona_color() + "\" /></td>";
                    filas += "<td>" + m[i].nombre + "</td>";
                    filas += "<td>(" + m[i].numGrupos + " grupos)</td>";
                    filas += "<td><button onclick=\"quitar_materia('" + m[i].idMateria + "')\">Quitar</button></td>";
                    filas += "</tr>\n";
                }
            }
            $("#tabla_materias").append(filas);
	});
}

// Borra la lista correspondiente de la tabla.
function quitar_materia(idMateria) {
    $('#fila_'+idMateria).remove();
}
