Genera tu horario

Aplicación web que recomienda horarios de clases para los alumnos de la carrera.
Genera todos los posibles horarios válidos (que no empalman) con las materias seleccionadas por el alumno.

Actualmente la base de datos está jarcodeada con algunas materias y clases.

Necesita PHP 5 y MySQL.