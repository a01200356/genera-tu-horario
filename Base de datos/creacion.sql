/*Base de datos para manejar los posibles horarios de los alumnos.*/

CREATE DATABASE Horarios;

CREATE TABLE Horarios.Materias
(
	idMateria int not null AUTO_INCREMENT,
	PRIMARY KEY (idMateria),
	nombre varchar(64)
);

CREATE TABLE Horarios.Grupos
(
	idGrupo int not null AUTO_INCREMENT,
	PRIMARY KEY (idGrupo),
	idMateria int not null,
	profesor varchar(64),
	periodo varchar(10)
);
ALTER TABLE Horarios.Grupos
	ADD FOREIGN KEY (idMateria)
	REFERENCES Horarios.Materias(idMateria)
	ON DELETE CASCADE;

CREATE TABLE Horarios.Clases
(
 	idClase int not null AUTO_INCREMENT,
	PRIMARY KEY (idClase),
	idGrupo int not null,
	dia varchar(2) not null,
	horaInicio varchar(5) not null,
	horaFin varchar(5) not null,
	salon varchar(10)
);
ALTER TABLE Horarios.Clases
	ADD FOREIGN KEY (idGrupo)
	REFERENCES Horarios.Grupos(idGrupo)
	ON DELETE CASCADE;