/* Materias */
INSERT INTO Horarios.Materias VALUES(1, "Desarrollo de Aplicaciones Web");
	INSERT INTO Horarios.Grupos VALUES(1, 1, "Eduardo Daniel Juárez", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(1, 1, "Ma", "11:30", "13:00", "6105");
		INSERT INTO Horarios.Clases VALUES(2, 1, "Mi", "08:00", "09:00", "6105");
		INSERT INTO Horarios.Clases VALUES(3, 1, "Vi", "11:30", "13:00", "6105");
			
INSERT INTO Horarios.Materias VALUES(2, "Interconexión de Redes");
	INSERT INTO Horarios.Grupos VALUES(2, 2, "José Oscar Hernández", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(4, 2, "Lu", "11:30", "13:00", "3304");
		INSERT INTO Horarios.Clases VALUES(5, 2, "Lu", "18:00", "20:00", "2104");
		INSERT INTO Horarios.Clases VALUES(6, 2, "Ju", "11:30", "13:00", "3304");
	INSERT INTO Horarios.Grupos VALUES(3, 2, "José Oscar Hernández", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(7, 3, "Lu", "11:30", "13:00", "3304");
		INSERT INTO Horarios.Clases VALUES(8, 3, "Ju", "18:00", "20:00", "2104");
		INSERT INTO Horarios.Clases VALUES(9, 3, "Ju", "11:30", "13:00", "3304");
			
INSERT INTO Horarios.Materias VALUES(3, "Sistemas Operativos");
	INSERT INTO Horarios.Grupos VALUES(4, 3, "José Oscar Hernández", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(10, 4, "Lu", "10:00", "11:30", "3304");
		INSERT INTO Horarios.Clases VALUES(11, 4, "Ma", "13:00", "14:00", "6104");
		INSERT INTO Horarios.Clases VALUES(12, 4, "Ju", "10:00", "11:30", "3304");

INSERT INTO Horarios.Materias VALUES(4, "Matemáticas III");
	INSERT INTO Horarios.Grupos VALUES(5, 4, "Luis Miguel Méndez", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(13, 5, "Lu", "16:00", "17:30", "3302");
		INSERT INTO Horarios.Clases VALUES(14, 5, "Ju", "16:00", "17:30", "3302");
	INSERT INTO Horarios.Grupos VALUES(6, 4, "Juan José Carracedo", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(15, 6, "Lu", "08:30", "10:00", "3201");
		INSERT INTO Horarios.Clases VALUES(16, 6, "Ju", "08:30", "10:00", "3201");
	INSERT INTO Horarios.Grupos VALUES(7, 4, "Juan José Carracedo", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(17, 7, "Lu", "11:30", "13:00", "3201");
		INSERT INTO Horarios.Clases VALUES(18, 7, "Ju", "11:30", "13:00", "3201");
	
INSERT INTO Horarios.Materias VALUES(5, "Administración e Innovación en Modelos de Negocios");
	INSERT INTO Horarios.Grupos VALUES(8, 5, "José Francisco Rocha", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(19, 8, "Ma", "10:00", "11:30", "3307");
		INSERT INTO Horarios.Clases VALUES(20, 8, "Vi", "10:00", "11:30", "3307");
		
INSERT INTO Horarios.Materias VALUES(6, "Ciudadanía y Democracia");
	INSERT INTO Horarios.Grupos VALUES(9, 6, "Mario Armando Vázquez", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(21, 9, "Ma", "16:00", "17:30", "4206");
		INSERT INTO Horarios.Clases VALUES(22, 9, "Vi", "16:00", "17:30", "4206");
		
INSERT INTO Horarios.Materias VALUES(7, "Compiladores");
	INSERT INTO Horarios.Grupos VALUES(10, 7, "Pedro Oscar Pérez", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(23, 10, "Ma", "11:30", "13:00", "6105");
		INSERT INTO Horarios.Clases VALUES(24, 10, "Mi", "08:00", "09:00", "6105");
		INSERT INTO Horarios.Clases VALUES(25, 10, "Vi", "11:30", "13:00", "6105");
		
INSERT INTO Horarios.Materias VALUES(8, "Seguridad Informática");
	INSERT INTO Horarios.Grupos VALUES(11, 8, "Agustín Domínguez", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(26, 11, "Lu", "11:30", "13:00", "3302");
		INSERT INTO Horarios.Clases VALUES(27, 11, "Ju", "11:30", "13:00", "3302");
		
INSERT INTO Horarios.Materias VALUES(9, "Lenguajes de Programación");
	INSERT INTO Horarios.Grupos VALUES(12, 9, "Gabriel Hermosillo", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(28, 12, "Ma", "07:00", "08:30", "2105");
		INSERT INTO Horarios.Clases VALUES(29, 12, "Vi", "07:00", "08:30", "2105");
		
INSERT INTO Horarios.Materias VALUES(10, "Proyectos de Desarrollo para Dispositivos Móviles");
	INSERT INTO Horarios.Grupos VALUES(13, 10, "María Elena Melón", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(30, 13, "Lu", "10:00", "11:30", "10101");
		INSERT INTO Horarios.Clases VALUES(31, 13, "Ju", "10:00", "11:30", "10101");
		
INSERT INTO Horarios.Materias VALUES(11, "Bases de datos avanzadas");
	INSERT INTO Horarios.Grupos VALUES(14, 11, "Enrique A Calderón", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(32, 14, "Ma", "07:00", "08:30", "3002");
		INSERT INTO Horarios.Clases VALUES(33, 14, "Vi", "07:00", "08:30", "3002");

INSERT INTO Horarios.Materias VALUES(12, "Programación Avanzada");
	INSERT INTO Horarios.Grupos VALUES(15, 12, "Pedro Oscar Pérez", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(34, 15, "Lu", "08:30", "10:00", "2105");
		INSERT INTO Horarios.Clases VALUES(35, 15, "Ju", "08:30", "10:00", "2105");
		
INSERT INTO Horarios.Materias VALUES(13, "Sistemas inteligentes");
	INSERT INTO Horarios.Grupos VALUES(16, 13, "Benjamín Valdés", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(36, 16, "Ma", "08:30", "10:00", "2101");
		INSERT INTO Horarios.Clases VALUES(37, 16, "Vi", "08:30", "10:00", "2101");
		
INSERT INTO Horarios.Materias VALUES(14, "Proyecto de Desarrollo de Videojuegos");
	INSERT INTO Horarios.Grupos VALUES(17, 14, "Eduardo V Rosado", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(38, 17, "Lu", "14:30", "16:00", "10201");
		INSERT INTO Horarios.Clases VALUES(39, 17, "Ju", "14:30", "16:00", "10201");
		
INSERT INTO Horarios.Materias VALUES(15, "Aplicaciones de Cómputo Móvil");
	INSERT INTO Horarios.Grupos VALUES(18, 15, "Amparo Correa", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(40, 18, "Ma", "17:30", "19:00", "2105");
		INSERT INTO Horarios.Clases VALUES(41, 18, "Vi", "17:30", "19:00", "2105");
		
INSERT INTO Horarios.Materias VALUES(16, "Formación Ciudadana y Compromiso Social");
	INSERT INTO Horarios.Grupos VALUES(19, 16, "Celine Laurence Roche", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(42, 19, "Mi", "10:00", "13:00", "2105");
		
INSERT INTO Horarios.Materias VALUES(18, "Estructura de Datos");
	INSERT INTO Horarios.Grupos VALUES(21, 18, "Eduardo Daniel Juárez", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(45, 21, "Ma", "08:30", "10:00", "6104");
		INSERT INTO Horarios.Clases VALUES(46, 21, "Vi", "08:30", "10:00", "6104");
		
INSERT INTO Horarios.Materias VALUES(19, "Bases de datos");
	INSERT INTO Horarios.Grupos VALUES(22, 19, "Ricardo Cortés", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(47, 22, "Ma", "17:30", "19:00", "2101");
		INSERT INTO Horarios.Clases VALUES(48, 22, "Vi", "17:30", "19:00", "2101");

INSERT INTO Horarios.Materias VALUES(20, "Álgebra lineal");
	INSERT INTO Horarios.Grupos VALUES(23, 20, "Ma. Griselda Tapia", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(49, 23, "Ma", "16:00", "17:30", "3204");
		INSERT INTO Horarios.Clases VALUES(50, 23, "Vi", "16:00", "17:30", "3204");
	INSERT INTO Horarios.Grupos VALUES(24, 20, "Ma. Griselda Tapia", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(51, 24, "Lu", "17:30", "19:00", "3204");
		INSERT INTO Horarios.Clases VALUES(52, 24, "Ju", "17:30", "19:00", "3204");
	INSERT INTO Horarios.Grupos VALUES(25, 20, "Ma. Griselda Tapia", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(53, 25, "Ma", "11:30", "13:00", "3302");
		INSERT INTO Horarios.Clases VALUES(54, 25, "Vi", "11:30", "13:00", "3302");
		
INSERT INTO Horarios.Materias VALUES(21, "Métodos Cuantitativos y Simulación");
	INSERT INTO Horarios.Grupos VALUES(26, 21, "Joaquín Galarza", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(55, 26, "Lu", "17:30", "19:00", "6104");
		INSERT INTO Horarios.Clases VALUES(56, 26, "Ju", "17:30", "19:00", "6104");
		
INSERT INTO Horarios.Materias VALUES(22, "Cine, Literatura y Cultura");
	INSERT INTO Horarios.Grupos VALUES(27, 22, "Angélica Camacho", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(57, 27, "Mi", "10:00", "13:00", "3204");
		
INSERT INTO Horarios.Materias VALUES(24, "Fundamentos de Redes");
	INSERT INTO Horarios.Grupos VALUES(29, 24, "Lizethe Pérez", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(60, 29, "Lu", "17:30", "19:00", "3305");
		INSERT INTO Horarios.Clases VALUES(61, 29, "Ju", "17:30", "19:00", "3305");
		INSERT INTO Horarios.Clases VALUES(62, 29, "Mi", "16:00", "17:30", "2105");
		
INSERT INTO Horarios.Materias VALUES(25, "Planeación de Microempresas para el Desarrollo Social");
	INSERT INTO Horarios.Grupos VALUES(30, 25, "Dulce Eloisa Saldaña", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(63, 30, "Sa", "09:00", "12:00", "3205");
		
INSERT INTO Horarios.Materias VALUES(26, "Estrategias y Competencias Académicas");
	INSERT INTO Horarios.Grupos VALUES(31, 26, "Yonatán Pérez", "Ene-May 14");
		INSERT INTO Horarios.Clases VALUES(64, 31, "Lu", "10:00", "11:30", "3001");
		INSERT INTO Horarios.Clases VALUES(65, 31, "Ju", "10:00", "11:30", "3001");